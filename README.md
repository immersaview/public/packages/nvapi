# NuGet packaging for NVAPI Repository #

## Example CMake Usage:
```cmake
target_link_libraries(target nvapi)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:nvapi,INTERFACE_INCLUDE_DIRECTORIES>")
```