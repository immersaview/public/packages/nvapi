cmake_minimum_required(VERSION 3.0)

# Declaring variables
set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}")

if ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "aarch64")
    set(ARCH_DIR "arm64")
elseif ("${CMAKE_SYSTEM_PROCESSOR}" MATCHES "AMD64|x86_64")
    if("${CMAKE_SIZEOF_VOID_P}" STREQUAL "4")
        set(ARCH_DIR "x86")
    elseif ("${CMAKE_SIZEOF_VOID_P}" STREQUAL "8")
        set(ARCH_DIR "x64")
    endif ()
else ()
    message(FATAL_ERROR "Unable to determine CPU architecture")
endif ()

if (UNIX)
    find_program(LSB_RELEASE_EXEC lsb_release)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} -d
        OUTPUT_VARIABLE LSB_RELEASE_DESC
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if (LSB_RELEASE_DESC MATCHES "Ubuntu")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/ubuntu16_04/gcc5_4_0/${ARCH_DIR}")
    elseif (LSB_RELEASE_DESC MATCHES "CentOS")
        set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/centos7/gcc7_2_1/${ARCH_DIR}")
    endif ()

elseif (WIN32)
    set(PLATFORM_DIR "${CMAKE_CURRENT_LIST_DIR}/windows/${ARCH_DIR}")
    if (${ARCH_DIR} MATCHES "x64")
        set(LIB_SUFFIX "64")
    endif ()
endif ()

function(add_imported_lib PLATFORM_DIR LIB)
    message(STATUS "Adding library '${LIB}'. Reference the library using the TARGET '${LIB}'")
    add_library("${LIB}" STATIC IMPORTED GLOBAL)
    set_property(TARGET "${LIB}" APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_CURRENT_LIST_DIR}/include")

    if (UNIX)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_RELEASE "${PLATFORM_DIR}/release/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION_DEBUG "${PLATFORM_DIR}/debug/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    elseif (WIN32)
        set_target_properties("${LIB}" PROPERTIES IMPORTED_LOCATION "${PLATFORM_DIR}/${CMAKE_STATIC_LIBRARY_PREFIX}${LIB}${LIB_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")
    endif ()
endfunction(add_imported_lib PLATFORM_DIR LIB)

add_imported_lib("${PLATFORM_DIR}" "nvapi")
